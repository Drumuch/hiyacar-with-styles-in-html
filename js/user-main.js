/* General Config */
/* *********************************************************************************/

var BASE_URL = '';
var calAction = ''; /* Default Calender action */
var dateAction = ''; /* Default Date Calender action (next month prev month) */
var startHours,startMinutes,endHours,endMinutes;

/* Window onload events */
/* *********************************************************************************/

$(window).load(function(){

	$('.jsOnly').css('display','block');

});



// Format Time Function, puts leading 0 into string
function formatTime(value)
{
	if(value <= 9)
	{
		return "0" + value;
	}
	else
	{
		return value;	
	}
}

// UNFormat Time Function, takes HH:MM turns into just miniutes
function unFormatTime(value)
{
	timeArray = value.replace(/^0+/, '').split(':');

	if(!timeArray[0])
	{
		return parseInt(timeArray[1]);
	}

	return (parseInt(timeArray[0]) * 60) + (parseInt(timeArray[1]));
}

				
$(document).ready(function() {


	if($('.secondary-location #addressInput').length)
	{
		$('.secondary-location #addressInput').on('blur','.required input[type="text"]',{ linkType:'required', cssClass:'.required' },validateInput);
		$('.secondary-location #addressInput .required input[type="text"]').each(function() {
			if($(this).val() == '')
			{
				$(this).closest('.required').addClass('has-errors');
			}
			else
			{
				$(this).closest('.required').addClass('ok');
			}
		});
	}


	if($('.photo-block .upload-photo').length || $('.profile .upload-photo').length)
	{
		$('.upload-photo').after('<div id="loadindicator" style="width:66px; height:66px;"></div>');
	}

	$('#dashboard #notification').css('overflow','auto');

	$('.photo-block').on('click', '.button', function() {
		if($('.photo-block #frm-image').val() != '')
		{
			$('.photo-block .upload-photo').hide();
			$('#loadindicator').html('<img src="/static/images/bg-upload-loader.gif" />').show();
		}
	});

	$('.profile .upload').on('click','.button', function() {
		if($('.profile #frm-image').val() != '')
		{
			$('.profile .upload .upload-photo').hide();
			$('#loadindicator').html('<img src="/static/images/bg-upload-loader.gif" />').css({'float':'left','margin-top':'10px'}).show();
		}
	});

	if($('.profile-description').length)
	{

    	var standard_message = 'Use this section to tell people more about yourself and why you use HiyaCar';

		if($('#frm-description').val() == "")
		{
			 $('#frm-description').val(standard_message);
		     $('#frm-description').css('color','#999');
		}

		$('#frm-description').focus(
		    function() {
		        if ($(this).val() == standard_message)
		        {
		            $(this).val("");
		            $(this).css('color','#333');
		        }
		    }
		);
		$('#frm-description').blur(
		    function() {
		        if ($(this).val() == "")
		        {
		            $(this).val(standard_message);
		            $(this).css('color','#999');
		        }
		    }
		);

		$('#frm-description').closest('form').on('submit',function() {
			if($('#frm-description').val() == standard_message)
			{
				$('#frm-description').val('');
			}
		});



	}
	
	if($('.account-vehicles_edit #frm-postcode').val() == '')
	{
		$('div.secondary').remove();
	}	

	$('.message-listing ul, .user-ratings ul, .payment-history ul').addClass('restrict');
	
	$("#notification, .profile .user-ratings ul, .message-listing ul").mCustomScrollbar({
		scrollButtons:{
			enable:true
		}
	});

	$('.notification label').css('padding-left','20px').append('<em class="alert">!</em>')

	// deal with anchoring the image
	$('.account-vehicles_edit input[type="image"]').live('click',function() {
	
		var formId = $(this).parents('section').attr('id');
		$(this).parents('form').attr('action','#' + formId);
	
	});
	
	if($('.account-vehicles_edit').length && window.location.hash)
	{
		$(window.location.hash).removeClass('contracted').addClass('expanded');	
	}
	
	$('.message-listing ul .f1').on('click',function() {
		$('input[type="checkbox"]',$(this)).attr('checked','checked');
	});
	
	$('.message-listing ul p').not('.f1').on('click',function() {
		window.location = $(this).parents('li').find('.f2 a').attr('href');
	});
	
	if(($('.secondary-location').length) && (!$('#secondary-address-detail').length))
	{
		$('.lookup').show();
	}
	
	$('.secondary-location a[href="#changeAddress"]').live('click',function() {
		$('#secondary-address-detail').slideUp('normal',function() {
			$('.lookup').slideDown();
		});
		$(this).hide();
	});
	
	$('#access-actions a').live('click',function() {
		var linkObj = $(this);
		$('#' + $(this).attr('data-rel')).slideDown('normal',function() {
			linkObj.slideUp();
		});
		return false;
	});


		
	// Function to parse Calender AJAX chekcboxes	
	function parseCheckboxes()
	{
		
		$( "#dialog-modal input[type='checkbox']" ).each(function() {
			
			if(this.checked)
			{
				$(this).parent('td').addClass('available');
			}
			else
			{
				$(this).parent('td').addClass('notavailable');
			}
			
		});
	}

	$('.expandable fieldset').wrap('<div class="inner" />');
	
	$('section.expandable').addClass('expanded');

	$('.expandable h1 a').live('click',function() {
	
		var formElement = $('.inner',$(this).parent('h1').parent('section'));
		var section = $(this).parent('h1').parent('section');
	
		if(formElement.is(":visible"))
		{
			formElement.slideUp('normal',function() {
				section.removeClass('expanded').addClass('contracted');									  
			});
		}
		else
		{
			formElement.slideDown('normal',function() {
				section.removeClass('contracted').addClass('expanded');									  
			});
		}
	
		return false;
	
	});

	/* Display Vehicle Availability Calender */
	$('#triggerCalendar').live('click',function() {
		
		dateAction = calAction = '/account/vehicles/edit/block-dates/id/' + $(this).attr('data-rel');
		
		$('#dialog-modal').load( calAction  + ' #block-dates', function(response, status, xhr) {

			parseCheckboxes();

			$( "#dialog-modal" ).dialog("open");
		});
		
		return false;
		
	});

	/* Handle next/prev calender months Calender */
	$('#dialog-modal #block-dates h2 .action').live('click',function() {
	
		var dateLinkItem = $(this).attr('href');
	
		$.ajax({type:'POST', url: dateAction, data:$('#dialog-modal form').serialize(), success: function(response) {
			
			dateAction = dateLinkItem;
			
			$('#dialog-modal').load( dateAction + ' #block-dates', function(response, status, xhr) {										
				
				calAction = dateAction;

				parseCheckboxes();
				
			});
			
		}});	
	
		return false;
		
	});
		
	/* Handle Change of calender availablility date */
	$( "#dialog-modal #block-dates td" ).live('click',function() {
	
		if($(this).hasClass('available'))
		{
			$(this).removeClass('available').addClass('notavailable');
			$(this).children('input[type="checkbox"]').attr('checked', false);
		}
		else if($(this).hasClass('notavailable'))
		{
			$(this).removeClass('notavailable').addClass('available');
			$(this).children('input[type="checkbox"]').attr('checked', true);
		}
											
	});
	
	/* Handle Submit of calender availablility dates */
	$( "#dialog-modal #block-dates input[type='image']" ).live('click',function() {
		$.ajax({type:'POST', url: calAction, data:$('#dialog-modal form').serialize(), success: function(response) {
			$( "#dialog-modal" ).dialog('close');
		}});										
		return false;
	});	

	/* Set visible default price fields based on hidden fields */
	$('#setPrice .price input[type="text"]').each(function() {
		if($(this).val() == '')
		{
			$(this).val($(this).siblings('.default').val());
		}
	});
	

	/* Show these items on load */
	$('.update-vehicle .action, .update-vehicle .reset').css('display','block');
	
	/* Increment 1 to Price Field */
	$('#setPrice .add').live('click',function() {
		
		var inputObj = $('input[type="text"]',$(this).parent('li'));
		
		if(inputObj.val() < 999)
		
			inputObj.val(parseInt(inputObj.val()) + 1);
		
		return false;
	});
	
	/* cut 1 to Price Field */
	$('#setPrice .minus').live('click',function() {
		
		var inputObj = $('input[type="text"]',$(this).parent('li'));
		
		if(inputObj.val() > 1)
		
			inputObj.val(parseInt(inputObj.val()) - 1);
		
		return false;
	});
	
	/* Reset Price Field */
	$('#setPrice .reset').live('click',function() {
		
		var inputObj = $('input[type="text"]',$(this).parent('li'));
		var recommendedObj = $('input[class="recommended"]',$(this).parent('li'));
		
		inputObj.val(parseInt(recommendedObj.val()));
		
		return false;
	});

	$('div.definedTimes input').hide();
	
	// Add container for Sliders
	$('<div class="slider-range"></div>').prependTo('div.definedTimes');	
	$('.availability-selector ul').addClass('interactive');
		


	$('#availability-template').on('click','a',function() {
		triggerTemplate($(this).attr('rel'));
		return false;
	});


	// Handle Check box selection	
	$('.availability-selector input[type="checkbox"]').live('click',function() { 
		
		var sliderWidth = $('.definedTimes').width();
		var sliderEl = $(this).parent('p').siblings('.definedTimes').children('.slider-range');
		var datarel = sliderEl.closest('li').attr('data-rel');
		var secondary = sliderEl.closest('ul').hasClass('secondary');
		var first = $(this).parent('p').siblings('.definedTimes').find('input').first();
		var last = $(this).parent('p').siblings('.definedTimes').find('input').last();
		
		// If its checked then its available so show the slider
		if($(this).is(':checked'))
		{
			
			if(!first.val())
			{
				first.val('00:00')	
			}
			if(!last.val())
			{
				last.val('24:00')	
			}
			sliderEl.slider({disabled: 0});
			if(secondary)
			{
				var datarel = sliderEl.closest('li').attr('data-rel');
				var relativeSlider = $('.primary li[data-rel="' + datarel + '"] .ui-slider-range');
				var blockWidth = Math.floor(sliderEl.find('.ui-slider-range').width());
				var relativeLeft = parseInt(relativeSlider.position().left);
				var blockLeft = parseInt(sliderEl.find('.ui-slider-range').position().left) - relativeLeft;
				$('.r-' + datarel + '').css({'left':blockLeft + 'px','width':blockWidth + 'px'});
			}
			
		}
		// NOPE its not checked and not available so hide the slider
		else
		{
			sliderEl.slider({disabled: 1});
			if(secondary)
			{
				$('.r-' + datarel + '').css({'left':'0px','width':'0'});
			}
		}
	
	})
	
	/* Rentals Screens AJAX ************************************** */
	$('.rentals h1 a').live('click',function() {
		var parent = $(this).closest('section');
		// no inner content so lets grab it from AJAX
		if(!parent.find('.inner').length)
		{
			var location = $(this).attr('href');
			parent.append('<div class="inner" />');
			parent.children('.inner').load( location + ' #booking-content', function(response, status, xhr) {
				parent.find('.inner').css({'position':'absolute','left':'-9999px'}).slideDown(1,function() {
					rebindBookingSliders(parent);			
					parent.find('.inner').slideUp(1,function() {
						parent.find('.inner').css({'position':'relative','left':'0'}).slideDown('normal');
						parent.removeClass('contracted').addClass('expanded');									  
							if ($.browser.msie) {
								$('.bookings-grid ').addClass('corners').append('<div class="corner tl" /><div class="corner tr" /><div class="corner bl" /><div class="corner br" />');
							}
					});						  
				});															  
			});
		}
		return false;
	});
	
	$('.rentals .tabs a').live('click',function() {
		var parentSection = $(this).closest('section');
		var parent = $(this).closest('.inner');
		// no inner content so lets grab it from AJAX
		var location = $(this).attr('href');

		parent.slideUp('normal',function() {
			parent.css({'position':'absolute','left':'-9999px'});
			parent.children('*').remove();
			parent.load( location + ' #booking-content', function(response, status, xhr) {
				parent.slideDown(1,function() {
					rebindBookingSliders(parentSection);			
					parent.slideUp(1,function() {
						parent.css({'position':'relative','left':'0'}).slideDown('normal');														 
							if ($.browser.msie) {
								$('.bookings-grid ').addClass('corners').append('<div class="corner tl" /><div class="corner tr" /><div class="corner bl" /><div class="corner br" />');
							}
					});						  
				});
			});															  
		});
		return false;
	});
	
	$('.rentals .listview .f1 a').live('click',function() {
		var parent = $(this).closest('li');
		var parentSection = $(this).closest('section');
		var bookingId = parent.attr('data-rel');
		var location = $(this).attr('href');
		var linkObject = $(this);
		if(parent.siblings('.fullview[data-rel="' + bookingId + '"]').length)
		{
			parent.siblings('.fullview[data-rel="' + bookingId + '"]').slideToggle('normal',function() {
				toggleIcon(linkObject,parent);});	
		}
		else
		{
			parent.after('<li data-rel="' + bookingId + '" class="fullview" style="display:none;" />')
	
			$('.fullview[data-rel="' + bookingId + '"]').load( location + ' .booking-details', function(response, status, xhr) {
				$('.fullview[data-rel="' + bookingId + '"]').slideDown('normal',function() {
					toggleIcon(linkObject,parent);});
					bindBookingMaps();
				
			});
		}


		return false;
	});
	/* Rentals Screens AJAX ************************************** */

	$('p').on('click','.inactive',false);


	// Disable on load until selected
	if(!$('.message-listing input[type="checkbox"]:checked').length)
	{
		$('.bin-selection').addClass('inactive').fadeTo(1,0.5);
	}

	$('.message-listing').on('change','input[type="checkbox"]',function() {
		if(!$('.message-listing input[type="checkbox"]:checked').length)
		{
			$('.bin-selection').addClass('inactive').fadeTo(1,0.5);
		}
		else
		{
			$('.bin-selection').removeClass('inactive').fadeTo(1,1);
		}

	});

	if($('#secondaryAddress').length)
	{
		$('#addressInput').hide();
	}

	$('.secondary-location').on('click','#changeAddress, #addAddress',function() {
		$('#currentAddress').slideUp();
		$('#addressInput').slideDown();
		return false
	});
	$('.secondary-location').on('click','#cancelChange',function() {
		$('#addressInput').slideUp();
		$('#addressInput input[type="text"]').val('');
		$('#addressLookupResponse').remove();
		$('#currentAddress').slideDown();
		return false
	});
	if (1 == $('#showAddress').val())
    {
		$('#addressInput').show();
		$('#currentAddress').hide();
    }

	function toggleIcon(LinkObject,parent)
	{
		if(LinkObject.children('img').attr('src') == '/static/images/user/img-expand.png')
		{
			LinkObject.children('img').attr('src','/static/images/user/img-contract.png');
			parent.addClass('selected');
		}
		else
		{
			LinkObject.children('img').attr('src','/static/images/user/img-expand.png');
			parent.removeClass('selected');
		}		
	}

	function rebindBookingSliders(parentSection)
	{
		return false;
	}
	
	triggerSliders();

	function triggerTemplate(template)
	{
		// template 1 = Available all day everyday
		// template 2 = Available all weekdays
		// template 3 = Available all weekend
		// template 4 = Available weekday evenings and weekends
		// template 5 = Available 9am – 5 pm weekdays

		// RESET

		$('#frm-primary-weekday0-available').attr('checked','checked');
		$('#frm-primary-weekday1-available').attr('checked','checked');
		$('#frm-primary-weekday2-available').attr('checked','checked');
		$('#frm-primary-weekday3-available').attr('checked','checked');
		$('#frm-primary-weekday4-available').attr('checked','checked');
		$('#frm-primary-weekday5-available').attr('checked','checked');
		$('#frm-primary-weekday6-available').attr('checked','checked');

		$('#frm-primary-weekday0-primary-start-time').siblings('.slider-range').removeClass('ui-state-disabled');
		$('#frm-primary-weekday1-primary-start-time').siblings('.slider-range').removeClass('ui-state-disabled');
		$('#frm-primary-weekday2-primary-start-time').siblings('.slider-range').removeClass('ui-state-disabled');
		$('#frm-primary-weekday3-primary-start-time').siblings('.slider-range').removeClass('ui-state-disabled');
		$('#frm-primary-weekday4-primary-start-time').siblings('.slider-range').removeClass('ui-state-disabled');
		$('#frm-primary-weekday5-primary-start-time').siblings('.slider-range').removeClass('ui-state-disabled');
		$('#frm-primary-weekday6-primary-start-time').siblings('.slider-range').removeClass('ui-state-disabled');

		$('#frm-primary-weekday0-primary-start-time').val('00:00');
		$('#frm-primary-weekday1-primary-start-time').val('00:00');
		$('#frm-primary-weekday2-primary-start-time').val('00:00');
		$('#frm-primary-weekday3-primary-start-time').val('00:00');
		$('#frm-primary-weekday4-primary-start-time').val('00:00');
		$('#frm-primary-weekday5-primary-start-time').val('00:00');
		$('#frm-primary-weekday6-primary-start-time').val('00:00');

		$('#frm-primary-weekday0-primary-end-time').val('24:00');
		$('#frm-primary-weekday1-primary-end-time').val('24:00');
		$('#frm-primary-weekday2-primary-end-time').val('24:00');
		$('#frm-primary-weekday3-primary-end-time').val('24:00');
		$('#frm-primary-weekday4-primary-end-time').val('24:00');
		$('#frm-primary-weekday5-primary-end-time').val('24:00');
		$('#frm-primary-weekday6-primary-end-time').val('24:00');

		$('#frm-secondary-weekday0-available').attr('checked',false);
		$('#frm-secondary-weekday1-available').attr('checked',false);
		$('#frm-secondary-weekday2-available').attr('checked',false);
		$('#frm-secondary-weekday3-available').attr('checked',false);
		$('#frm-secondary-weekday4-available').attr('checked',false);
		$('#frm-secondary-weekday5-available').attr('checked',false);
		$('#frm-secondary-weekday6-available').attr('checked',false);



		if(template == 2)
		{

			$('#frm-primary-weekday0-available').attr('checked',false);
			$('#frm-primary-weekday6-available').attr('checked',false);

			$('#frm-primary-weekday0-primary-start-time').siblings('.slider-range').addClass('ui-state-disabled');
			$('#frm-primary-weekday6-primary-start-time').siblings('.slider-range').addClass('ui-state-disabled');

		}

		if(template == 3)
		{

			$('#frm-primary-weekday1-available').attr('checked',false);
			$('#frm-primary-weekday2-available').attr('checked',false);
			$('#frm-primary-weekday3-available').attr('checked',false);
			$('#frm-primary-weekday4-available').attr('checked',false);
			$('#frm-primary-weekday5-available').attr('checked',false);

			$('#frm-primary-weekday1-primary-start-time').siblings('.slider-range').addClass('ui-state-disabled');
			$('#frm-primary-weekday2-primary-start-time').siblings('.slider-range').addClass('ui-state-disabled');
			$('#frm-primary-weekday3-primary-start-time').siblings('.slider-range').addClass('ui-state-disabled');
			$('#frm-primary-weekday4-primary-start-time').siblings('.slider-range').addClass('ui-state-disabled');
			$('#frm-primary-weekday5-primary-start-time').siblings('.slider-range').addClass('ui-state-disabled');

		}

		if(template == 4)
		{

			$('#frm-primary-weekday0-primary-start-time').val('00:00');
			$('#frm-primary-weekday1-primary-start-time').val('17:30');
			$('#frm-primary-weekday2-primary-start-time').val('17:30');
			$('#frm-primary-weekday3-primary-start-time').val('17:30');
			$('#frm-primary-weekday4-primary-start-time').val('17:30');
			$('#frm-primary-weekday5-primary-start-time').val('17:30');
			$('#frm-primary-weekday6-primary-start-time').val('00:00');

			$('#frm-primary-weekday0-primary-end-time').val('24:00');
			$('#frm-primary-weekday1-primary-end-time').val('24:00');
			$('#frm-primary-weekday2-primary-end-time').val('24:00');
			$('#frm-primary-weekday3-primary-end-time').val('24:00');
			$('#frm-primary-weekday4-primary-end-time').val('24:00');
			$('#frm-primary-weekday5-primary-end-time').val('24:00');
			$('#frm-primary-weekday6-primary-end-time').val('24:00');
		}

		if(template == 5)
		{

			$('#frm-primary-weekday0-available').attr('checked',false);
			$('#frm-primary-weekday6-available').attr('checked',false);

			$('#frm-primary-weekday0-primary-start-time').siblings('.slider-range').addClass('ui-state-disabled');
			$('#frm-primary-weekday6-primary-start-time').siblings('.slider-range').addClass('ui-state-disabled');

			$('#frm-primary-weekday1-primary-start-time').val('9:00');
			$('#frm-primary-weekday2-primary-start-time').val('9:00');
			$('#frm-primary-weekday3-primary-start-time').val('9:00');
			$('#frm-primary-weekday4-primary-start-time').val('9:00');
			$('#frm-primary-weekday5-primary-start-time').val('9:00');

			$('#frm-primary-weekday1-primary-end-time').val('17:00');
			$('#frm-primary-weekday2-primary-end-time').val('17:00');
			$('#frm-primary-weekday3-primary-end-time').val('17:00');
			$('#frm-primary-weekday4-primary-end-time').val('17:00');
			$('#frm-primary-weekday5-primary-end-time').val('17:00');
		}

		$(".slider-range").slider("destroy"); 
		triggerSliders();

	}

	function triggerSliders()
	{

		// Instantiate each slider on screen 
		$( ".slider-range" ).each(function() {
				
				var isDisabled = !$(this).parent('.definedTimes').siblings('p').children('input[type="checkbox"]:checked').length;
				var secondary = $(this).closest('ul').hasClass('secondary');
				var defaultStart = 0;
				var defaultEnd = 1440;
				var sliderWidth = $('.definedTimes').width();
				var rel = $(this).closest('li').attr('data-rel');
				var datarel = $(this).closest('li').attr('data-rel');
				var sliderParent = $(this);
				
				if($(this).siblings('input').first().val())
				{
					defaultStart = unFormatTime($(this).siblings('input').first().val());
					//alert($(this).siblings('input').first().attr('name') + ' - ' + $(this).siblings('input').first().val() + ' - ' + defaultStart);
				}
				
				
				if($(this).siblings('input').last().val())
				{
					defaultEnd = unFormatTime($(this).siblings('input').last().val());
				}
				
				var currentLeft = 0;
				
				$(this).slider({
					range: true,
					min: 0,
					max: 1440,
					step: 15,
					disabled: isDisabled,
					values: [ defaultStart, defaultEnd ],
					change: function(event, ui) {
											
						// Update Inputs 					
						startHours = Math.floor(ui.values[0] / 60);
						startMinutes = ui.values[0] %60;
						
						endHours = Math.floor(ui.values[1]  / 60);
						endMinutes = ui.values[1] %60;


					
						sliderParent.siblings('input').first().val( formatTime(startHours) + ":" + formatTime(startMinutes));
						sliderParent.siblings('input').last().val( formatTime(endHours)  + ":" + formatTime(endMinutes));
						
						// If its a secondary 					
						if(secondary)
						{
							var relativeSlider = $('.primary li[data-rel="' + datarel + '"] .ui-slider-range');
							var blockWidth = Math.floor($(this).find('.ui-slider-range').width());
							var relativeLeft = parseInt(relativeSlider.position().left);
							var blockLeft = parseInt($(this).find('.ui-slider-range').position().left) - relativeLeft;
							$('.r-' + datarel + '').css({'left':blockLeft + 'px','width':blockWidth + 'px'});
						}
						
						if(!secondary)
						{
							var relativeSlider = $('.secondary li[data-rel="' + datarel + '"] .ui-slider-range');
							var relativeLeft = parseInt(relativeSlider.position().left);
							var blockLeft = relativeLeft - parseInt($(this).find('.ui-slider-range').position().left);
							$('.r-' + datarel + '').css({'left':blockLeft + 'px'});
						}
											
					},
					create: function() {
						if(secondary)
						{
							var relativeSlider = $('.primary li[data-rel="' + datarel + '"] .ui-slider-range');
							var relativeSliderEl = $('.primary li[data-rel="' + datarel + '"] .ui-slider-range')[0];
							var thisSliderEl = $('.secondary li[data-rel="' + datarel + '"] .ui-slider-range')[0];
							var blockWidth = Math.floor(sliderWidth * parseFloat(thisSliderEl.style.width) / 100);
							var relativeLeft = Math.floor(sliderWidth * parseFloat(relativeSliderEl.style.left) / 100);
							var blockLeft = Math.floor(sliderWidth * parseFloat(thisSliderEl.style.left) / 100) - relativeLeft;

							if(isDisabled)
							{
								blockLeft = 0;
								blockWidth = 0;
							}
							relativeSlider.append('<div class="unavailable-range r-' + datarel + '" style="left:' + blockLeft + 'px; width:' + blockWidth + 'px;"></div>');

						}
					}
				});
				
				
				
		});

	}
					
	
	/* Payment History JS */
	$('.payment-history .payments ul').mCustomScrollbar({
		scrollButtons:{
			enable:true
		}
	});	
	
	$('.payment-history article').last().hide();
	$('.payment-history .tabs li:first-child').addClass('selected');
	$('.payment-history .tabs a').live('click',function() {
		var rel = $(this).attr('data-rel');
		$('.payment-history .tabs li').removeClass('selected');
		$(this).parent('li').addClass('selected');
		$('.payment-history article').hide();
		$('.payment-history article[data-rel="' + rel + '"]').show();
		return false;
	})
	/* Payment History JS */


	// Perform some preliminary validation on the form fields before posting to payment gateway.
	function checkCardDetails()
	{
		var doSubmit = true;
		var msg = '';
		var pan = $('#frm-pan').val();
		if ('' == pan) {
			msg += 'Sorry, you need to enter a valid Card Number before continuing.\n';
			doSubmit = false;
		}
		if (0 <= pan.indexOf(' ')) {
			$('#frm-pan').val(pan.replace(/\s/gi, ''));
		}
		pan = pan.replace(/\s/gi, '');
		if (19 < pan.length) {
			msg += 'Sorry, you need to enter a valid Card Number before continuing.\n';
			doSubmit = false;
		}
		// Ensure nothing other than numerics in the pan.
		var regex = new RegExp('^[0-9]*$');
		if(!regex.test(pan)) {
			msg += 'Sorry, you need to enter a valid Card Number before continuing.\n';
			doSubmit = false;
		} else {
			// Luhn check the pan.
			var sums = [[0,1,2,3,4,5,6,7,8,9], [0,2,4,6,8,1,3,5,7,9]];
			var sum = 0;
			var transpose = 0;
			for (var i = pan.length - 1; i >= 0; i--) {
				sum += sums[transpose++ & 1][pan[i]];
			}
			if (sum % 10 != 0) {
				msg += 'Sorry, you need to enter a valid Card Number before continuing.\n';
				doSubmit = false;
			}
		}
		var cn = $('#frm-cardholdername').val();
		if ('' == cn) {
			msg += 'Sorry, you need to enter a Cardholder Name before continuing.\n';
			doSubmit = false;
		}
		var em = $('#frm-expirymonth').val();
		if ('0' == em) {
			msg += 'Sorry, you need to enter a \'Valid to\' month before continuing.\n';
			doSubmit = false;
		}
		var ey = $('#frm-expiryyear').val();
		if ('0' == ey) {
			msg += 'Sorry, you need to enter a \'Valid to\' year before continuing.\n';
			doSubmit = false;
		}
		var d = new Date();
		var m = d.getMonth() + 1;
		var y = d.getFullYear();
		ey = $('#frm-expiryyear option[value="' + ey + '"]').text();
		em = $('#frm-expirymonth option[value="' + em + '"]').text();
		var sy = $('#frm-startyear option[value="' + $('#frm-startyear').val() + '"]').text();
		var sm = $('#frm-startmonth option[value="' + $('#frm-startmonth').val() + '"]').text();
		if (ey && em && (y > ey || (y == ey && m > em))) {
			msg += 'Sorry, we\'re sorry but this card has already expired. Please enter a different card to continue.\n';
			doSubmit = false;
		}
		if (sy && sm && (y < sy || (y == sy && m < sm))) {
			msg += 'Sorry, the start date you have entered is not valid.\n';
			doSubmit = false;
		}
		if ('' != ey && '' != sy) {
			if (sy > ey) {
				msg += 'Sorry, \'Valid to\' month and year cannot be earlier than \'Valid from\' month and year.\n';
				doSubmit = false;
			} else {
				if ('' != sm && '' != em) {
					if (sy + sm > ey + em) {
						msg += 'Sorry, \'Valid to\' month and year cannot be earlier than \'Valid from\' month and year.\n';
					}
				}
			}
		}
		var isn = $('#frm-issuenumber').val();
		if ('' != isn && (!regex.test(isn) || 0 == isn)) {
			msg += 'Please enter a valid Issue Number to continue.\n';
			doSubmit = false;
		}
		if (!doSubmit) {
			alert(msg);
		}
		return doSubmit;
	}


	if($('#save-details').length)
	{
		$('#save-details').live('click', function() {
			return checkCardDetails();
		});
	}


	$('#continue-step-4').live('click', function() {
		return checkCardDetails();
	});
});


