var letters = new Array('a','b','c','d','e','f','g','h','i','j');

var WEB_ROOT = ((document.location.protocol == 'https:') ? 'https://' : 'http://') + document.location.host;
var hasValidValue = 0;
var clicked = false;

function setCookie(c_name,value,exdays) {
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name) {
	var i,x,y,ARRcookies=document.cookie.split(";");
	for (i=0;i<ARRcookies.length;i++) {
		x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
		y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
		x=x.replace(/^\s+|\s+$/g,"");
		if (x==c_name) return unescape(y);
	}
}

$(function() {

	if($('#frm-too-new-for-service').length && $('#frm-too-new-for-service').is(':checked'))
	{
		$('.dates').hide();
	}

	$('.further-detail').on('click','#frm-too-new-for-service',function(){

		if($('#frm-too-new-for-service').is(':checked'))
		{
			$('.dates select option').removeAttr('selected');
			$('.dates .select').each(function() {
				var text = $(this).find('option[value=""]').attr('label');
				$(this).find('.list-heading em').text(text);
			});
			$('.dates').slideUp();
		}
		else
		{
			$('.dates').slideDown();
		}

	});

	var ua=navigator.userAgent.toLowerCase();
	var client={isIE:ua.indexOf("msie")>-1,isIE6:ua.indexOf("msie 6")>-1};
	$('body').prepend('<div id="sk-notices" style="z-index:9999;"></div>');
	if (client.isIE&&client.isIE6) {
		var IElink = 'http://www.microsoft.com/windows/downloads/ie/getitnow.mspx';
		var Firefoxlink = 'http://www.mozilla.com/firefox/';
		var Chromelink = 'http://www.google.com/chrome/';
		$('#sk-notices').append('<div style="background-color:#F9B8B8;border-bottom:2px solid #DEA4A4;font-family:Verdana,sans-serif;font-size:12px;text-align:center;color:#333;"><p style="padding:6px 15px;margin:0;">You are using an out-of-date and unsupported version of Internet Explorer, <a style="text-decoration:underline;color:#0D394D;font-weight:bold;" target="_blank" href="'+IElink+'">click here</a> to download the latest version. Fancy a better browser? Try <a style="text-decoration:underline;color:#0D394D;font-weight:bold;" target="_blank" href="'+Firefoxlink+'">Mozilla Firefox</a> or <a style="text-decoration:underline;color:#0D394D;font-weight:bold;" target="_blank" href="'+Chromelink+'">Google Chrome</a></p></div>');
	}
	if (getCookie("username") == null || getCookie("username") == "") {
		var Policylink = WEB_ROOT + '/privacy-policy';
		var Cookieslink = 'http://civicuk.com/cookie-law/browser-settings';
		$('#sk-notices').append('<div style="background-color:#E8FDCE;border-bottom:2px solid #8BC541;font-family:Verdana,sans-serif;font-size:12px;text-align:center;color:#333;position:relative;"><p style="padding:6px 50px 6px 10px;margin:0;">This website uses cookies to store your data, for more information see our <a style="text-decoration:underline;color:#0D394D;font-weight:bold;" href="'+Policylink+'">cookie policy</a> and <a style="text-decoration:underline;color:#0D394D;font-weight:bold;" target="_blank" href="'+Cookieslink+'">change your settings</a> at any time <a href="#" id="sk-cc-close" style="text-decoration:none;color:#0D394D;font-weight:bold;position:absolute;top:6px;right:10px;">Close</a></p></div>');
	}
	$('#sk-notices').on('click', '#sk-cc-close', function() {
		setCookie("username", true, 365);
		$(this).parents('div').eq(0).slideUp();
		return false;
	});

	doDialog();
	
	$('.jsOnly').css('display','block');	

	$('body').on('click','.inactive',false);

	$('.hideOnLoad, .availability-search_vehicle .availability, .how-panel section:not(:first-child), .how-panel article div').hide();

	$('.hideOnLoad .errors').each(function(){
		var p = $(this).parents('.hideOnLoad');
		$('a[data-rel="' + p.attr('id') + '"]').hide();
		p.show();
	});
		   
	if ($.browser.msie) {
		$('#my-car #tips, .primary li a, #add-vehicle .price-matrix, .secondary li a, header .auth, .home form, .home #main, .home .banner, .layout-b .panel-a, .layout-f .panel-b, .how-panel, .how-panel article, .availability-selector, .layout-f .panel-a, .layout-g .panel-a, .masthead, .payment-history, .payment-history article, .layout-f .panel-a .user-ratings, .message-listing, .bookings-grid, #dashboard #rent-your-car, #support-message, #dashboard #rent-a-car, #dashboard #my-car').addClass('corners').append('<div class="corner tl" /><div class="corner tr" /><div class="corner bl" /><div class="corner br" />');
		$('#main').addClass('corners').append('<div class="corner blm" /><div class="corner brm" />');
	}

	$('.data-input fieldset span, .date-field').on('click','input',function() {
		$(this).siblings('ul.errors').slideUp();
		$(this).parents('section').children('ul.errors').slideUp();
	});

	$('.data-input fieldset').on('click','.terms',function() {
		$(this).siblings('ul.errors').slideUp();
	});
	
    $('.availability-search_vehicle .images img').each(function() {
		$(this).parent('a').attr('href',$(this).attr('data-full-url'));
	}); 

	$('.availability-search_vehicle .images a').lightBox({
		imageLoading: '/static/images/bg-ajax-loader.gif',
		imageBtnClose: '/static/images/btn-close.gif'
   	})

	$('#frm-password, #frm-new-password').closest('div').after('<div id="password-strength"><p id="password-strength-result" class="w3c"></p><em id="graybar"></em><em id="colourbar"></em><p id="password-strength-label">Password strength</p></div>');
	var perc = 0 ;
	$('#frm-password, #frm-new-password').keyup(function(){
		$('#password-strength-result').html(passwordStrength($(this).val(),''));
		perc = passwordStrengthPercent($(this).val(),'');
		$('#colourbar').css({ width: (perc * 1.3) + 'px' })
	})
	if($('#frm-password').length)
	{
		$('#password-strength-result').html(passwordStrength($('#frm-password').val(),''));
		perc = passwordStrengthPercent($('#frm-password').val(),'');
		$('#colourbar').css({ width: (perc * 1.3) + 'px' })
	}

	$('form.user-registration').on('blur','.required input[type="text"]',{ linkType:'required', cssClass:'.required' },validateInput);
	$('form.user-registration').on('blur','.emailaddress input',{ linkType:'email', cssClass:'.emailaddress' },validateInput);
	$('form.user-registration').on('blur','.date input',{ linkType:'date', cssClass:'.date' },validateInput);
	$('form.user-registration').on('blur','.dob input',{ linkType:'dob', cssClass:'.dob' },validateInput);
	$('form.user-registration').on('blur','.licence input',{ linkType:'licence', cssClass:'.licence' },validateInput);
	$('form.user-registration').on('blur','.mobile input',{ linkType:'mobile', cssClass:'.mobile' },validateInput);
	$('form.user-registration').on('blur','.password input',{ linkType:'password', cssClass:'.password' },validateInput);

	


	$('body').on('click','.list-heading',function() {
		$(this).siblings('ul').show();										
	})

	if($('.stage1').length)
	{
		$('input').on('change',function(){
			if($('input[type="text"]').val() != '' && $('input[type="checkbox"]').attr('checked') == 'checked')
			{
				$('.stage1 .continue-step-2').removeClass('inactive').fadeTo(1,1);
			}
			else
			{
				$('.stage1 .continue-step-2').addClass('inactive').fadeTo(1,0.5);
			}
		});
		if (!$('input[type="checkbox"]:not(:checked)').length && !$('input[type="text"][value=""]').length)
		{
			$('.stage1 .continue-step-2').removeClass('inactive').fadeTo(1,1);
		}
		else
		{
			$('.stage1 .continue-step-2').addClass('inactive').fadeTo(1,0.5);
	
		}
		
	}

	if($('.stage2').length)
	{

		$('input').on('change',function(){
			if (!$('input[type="checkbox"]:not(:checked)').length)
			{
				$('.stage2 .continue-step-3').removeClass('inactive').fadeTo(1,1);
			}
			else
			{
				$('.stage2 .continue-step-3').addClass('inactive').fadeTo(1,0.5);
		
			}
		});
		
		if (!$('input[type="checkbox"]:not(:checked)').length)
		{
			$('.stage2 .continue-step-3').removeClass('inactive').fadeTo(1,1);
		}
		else
		{
			$('.stage2 .continue-step-3').addClass('inactive').fadeTo(1,0.5);
		}

		if($('.claims-history .claim').length || $('h3.failed').length)
		{
			$('.stage2 .continue-step-3').addClass('inactive').fadeTo(1,0.5);
		}
	}

	// Helper for Driving licence issue date
	$('.licence label').on('click','em.help',function() {

		$("#dialog-modal").html('<div class="helper"><em class="close">Close</em><h1>Licence Issue Date...</h1><h2>How to find it on your driving licence.</h2><p><img src="/static/images/img-licence-dates.jpg" alt="" /></p></div>');
		$("#dialog-modal").dialog({
            autoOpen:false,
            modal: true,
            resizable: true,
            draggable: true,
            closeOnEscape: true,
            position: ['center',20],
            title: "Trazoo"}).dialog("open");
		$('.helper').on('click','.close',function(){
			$("#dialog-modal").dialog("close");
		});

	});

	// Helper for Driving licence issue date
	$('.licence-num label').on('click','em.help',function() {

		$("#dialog-modal").html('<div class="helper"><em class="close">Close</em><h1>Licence Number...</h1><h2>How to find it on your driving licence.</h2><p><img src="/static/images/img-licence-number.jpg" alt="" /></p></div>');
		$("#dialog-modal").dialog('option','width',600).dialog("open");
		$('.helper').on('click','.close',function(){
			$("#dialog-modal").dialog("close");
		});

	});

	// Helper for Driving licence issue date
	$('.date label').on('click','em.help',function() {

		$("#dialog-modal").html('<div class="helper"><em class="close">Close</em><h1>Licence Issue Date...</h1><h2>How to find it on your driving licence.</h2><p><img src="/static/images/img-licence-dates.jpg" alt="" /></p></div>');
		$("#dialog-modal").dialog("open");
		$('.helper').on('click','.close',function(){
			$("#dialog-modal").dialog("close");
		});

	});

	// Helper for Mobile Field
	$('.mobile label').on('click','em.help',function() {

		$("#dialog-modal").html('<div class="helper"><em class="close">Close</em><h1>Mobile Phone Number...</h1><h2>We need your mobile number so that we can contact you with specific information about your car rentals with HiyaCar. We don\'t use this for advertising and never pass it on to third parties.</h2></div>');
		$("#dialog-modal").dialog("open");
		$('.helper').on('click','.close',function(){
			$("#dialog-modal").dialog("close");
		});

	});

	if($('#warningPanel').length) {

		$("#dialog-modal").html('<div id="warningPanel"><em class="close">Close</em>' + $('#warningPanel').html() + '</div>');
		$("#dialog-modal").dialog("open");
		$('.layout-a > #warningPanel').remove();
		$('#dialog-modal').on('click','.close, .overlayclose',function(){
			$("#dialog-modal").dialog("close");
		});

	}

	$(window).on('mouseleave','.select',function() {
		$(this).find('ul').hide();									
	})

	if($('.reviews li').length <= 1)
	{
		$('.review-actions').hide();
	}

	$('.review-actions').on('click','.next',function() {

		var current = $('.reviews li.active');
		var next = current.next('li');
		var first = $('.reviews li:first-child');

		current.hide().removeClass('active');
		if(next.length) { next.show().addClass('active'); }
		else { first.show().addClass('active'); }
		
		return false;
	});

	$('.review-actions').on('click','.prev',function() {

		var current = $('.reviews li.active');
		var prev = current.prev('li');
		var last = $('.reviews li:last-child');

		current.hide().removeClass('active');
		if(prev.length) { prev.show().addClass('active'); }
		else { last.show().addClass('active'); }
		
		return false;
	});

	// Live event required as re-bind with AJAX in place, .on not supporting
	$('.select a').live('click',function(event) {
		$(this).parents('ul').find('a').removeClass('selected');	
		$(this).parents('ul').siblings('.list-heading').children('em').text($(this).text());
		$(this).addClass('selected');	
		$(this).parents('ul').hide();	
		$(this).parents('div').siblings('select').children('option').removeAttr('selected');
		$(this).parents('div').siblings('select').children('option[value="'+ $(this).attr('rel') + '"]').attr('selected','selected');
		return false;
	})
	
	
	$(window).on('click','input[name="action[addressLookup]"]',function() {

		var value = $('#frm-postcode').val();
		if(!value) {
			var value = $('#frm-postcode-fb').val();
		}

		if(!value) return false;

		var parent = $(this).parent('span').parent('div');

		var selectTop = '<div id="addressLookupResponse"><label class="select-label">Select an address, or enter the address below:</label><div class="select lg"><select id="frm-address" name="address" style="display: none;"><option label="Select an address" value="">Select an address</option>';
		var selectBase = '</select>';
		var selectAppend = '';
		var selectHTMLTop = '<div><p class="list-heading"><em>Select an address</em></p><ul data-rel="frm-address" style="display: none;"><li><a rel="" href="#Select an address">Select an address</a></li>';
		var selectHTMLBase = '</ul></div></div></div>';
		var selectHTMLAppend = '';

		$.ajax({
			url: '/api/postcode-addresses/postcode/' + value,
			dataType: 'json',
			success: function(data) {
				$.each(data.result, function(key, value) {
					selectAppend += '<option value="' + value['id'] + '">' +value['streetAddress'] + '</option>';
					selectHTMLAppend += '<li><a rel="' + value['id'] + '" href="#' + value['streetAddress'] + '">' + value['streetAddress'] + '</a></li>';
				});
				
				if(!$('#addressLookupResponse').length)
				{
					parent.after(selectTop + selectAppend + selectBase + selectHTMLTop + selectHTMLAppend + selectHTMLBase);
					$('#hiddenAddressFields').show();
				}
				else
				{
					$('#addressLookupResponse').replaceWith(selectTop + selectAppend + selectBase + selectHTMLTop + selectHTMLAppend + selectHTMLBase);
					$('#hiddenAddressFields').show();
				}
				
				bindAddressLookup();
			
			}
		});
		
		return false;
	});

	$('.user-ratings').css('overflow','hidden');

	/* How Panel JS */
	$('#hownav li:first-child').addClass('selected');
	$('#hownav').on('click','a',function() {
		var rel = $(this).attr('data-rel');
		$('#hownav li').removeClass('selected');
		$(this).parent('li').addClass('selected');
		$('.how-panel section').hide();
		$('.how-panel section[data-rel="' + rel + '"]').show();
		return false;
	})
	$('.how-panel article').addClass('expandable contracted');
	$('.how-panel').on('click','article h3 em',function() {
		$(this).parent('h3').siblings('div').slideToggle();
		$(this).parent('h3').parent('article').toggleClass('contracted');
	})
		   
	$( '#when-datepicker' ).datepicker();
	
	if($('#frm-search-start-at').length)
	handleDateRangePickers($('#frm-search-start-at'),$('#frm-search-end-at'),false);
	
	if($('#frm-vehicle-booking-start-at').length)
	handleDateRangePickers($('#frm-vehicle-booking-start-at'),$('#frm-vehicle-booking-end-at'),$('#bookingVehicleId').attr('data-rel'));

	if($('#frm-booking-start-at').length)
	handleDateRangePickers($('#frm-booking-start-at'),$('#frm-booking-end-at'),$('#bookingVehicleId').attr('data-rel'));
	
	if($('#frm-start-at').length)
	handleDateRangePickers($('#frm-start-at'),$('#frm-end-at'),false);

	$('#frm-last-serviced-at').datepicker({dateFormat: "dd/mm/yy"});

	$( '.date' ).on('click',function() {
		$(this).siblings('input[type="text"]').click();	
		return false;
	});

	var count = 0;
	var delay = 200;
	$( '.home #main .feature' ).each(function() {
	
		var fTop = $(this).css('top');
		$(this).css('top','-300px');
		$(this).delay(count*delay).animate({top:fTop},1000,'easeOutBounce');
		count++;
	
	})

	var bookingFormLocation;

	$('#dialog-modal').on('click','.action input:not(.confirm)',function() {
		//Close the dialog
		$("#dialog-modal").dialog("close");
		return false;
	});  	
	
	if ($('body').hasClass('availability-search_vehicle')) {
		var match = location.pathname.match(/\/id\/(\d+)/);
		if (match) {
			$.ajax('/api/visit-vehicle/id/' + match[1]);
		}
	}
	
	if ($('body').hasClass('availability-search_index') && $('#map').length || $('body').hasClass('availability-search_view-shortlist') && $('#map').length) {
		var map = new google.maps.Map(document.getElementById('map'), {
		  zoom: 10,
		  center: new google.maps.LatLng(-33.92, 151.25),
		  mapTypeId: google.maps.MapTypeId.ROADMAP,
		  scrollwheel: false
		});
		
		var latlngbounds = new google.maps.LatLngBounds();
		var marker, i;
		var count = 0;
		
		$('.vehicle').each(function() {
		
			var parent = $(this);
			var thisLat = $(this).attr('data-lat');
			var thisLng = $(this).attr('data-lng');
			var thisAction = $(this).find('.view-details').attr('href');
			var location = new google.maps.LatLng(thisLat, thisLng);
			
			latlngbounds.extend(location);
		
			marker = new google.maps.Marker({
				position: location,
				map: map
			});
			
			if(count <= 9)
			{
				marker.setIcon('/static/images/bg-map-icon-' + letters[count] + '.png', [32,41]); 
			}
			else
			{
				marker.setIcon('/static/images/bg-map-icon-more.png', [32,41]); 
			}
			
			map.setCenter(latlngbounds.getCenter());
			map.fitBounds(latlngbounds); 
			
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
			  window.location = thisAction;
			}
			})(marker, i));
			
			google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
			return function() {
			  parent.addClass('highlight');
			}
			})(marker, i));
		
			
			google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
			return function() {
			  parent.removeClass('highlight');
			}
			})(marker, i));
			
			count ++;
		
		});
		$('#map').stickySidebar({constrain: true});
	}
	
	if ($('body').hasClass('availability-search_vehicle')) {
		var myLatlng = new google.maps.LatLng($('#map').attr('data-lat'), $('#map').attr('data-lng'));
		var myOptions = {
			zoom: 12,
			center: myLatlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			scrollwheel: false
		}

		var map = new google.maps.Map(document.getElementById("map"), myOptions);

		var marker = new google.maps.Marker({
			position: myLatlng, 
			map: map
		});   
		
		marker.setIcon('/static/images/bg-map-icon-a.png', [32,41]); 
	}

	if ($('body').hasClass('account-rentals_index') && $('.map').length) {
		bindBookingMaps();
	}

	if($('#resultCount').length)
	{
		$('#resultCount').after('<form id="filter"><label id="filterLabel">Ordered by:</label></form><form id="filter2"><label id="filterLabel2">Distance:</label></form>');
		$('#sortFilter').clone().appendTo('#filter');
		$('#distanceFilter').clone().appendTo('#filter2');

		$('#filter').on('click','a',function() {
			$('#sortFilter option[value="'+ $(this).attr('rel') + '"]').attr('selected','selected');
			$('form.search').submit();
			return false;
		});

		$('#filter2').on('click','a',function() {
			$('#distanceFilter option[value="'+ $(this).attr('rel') + '"]').attr('selected','selected');
			$('form.search').submit();
			return false;
		});
	}
	
	$('.search').on('click','ul[data-rel="frm-search-make-id"] a', function() {
		var makeId = $(this).attr('rel');
		var modelField = $('#frm-search-model-id');
		var modelSelect = $('ul[data-rel="frm-search-model-id"]');
		var modelSelectTitle = $('ul[data-rel="frm-search-model-id"]').siblings('.list-heading').children('em');
	
		if (makeId) {
			$.getJSON('/api/models/makeId/' + makeId, function(data) {
					
				modelField.html('');	
				modelSelect.html('');
				modelSelectTitle.text('All models');
				
				$.each(data.result, function(key, value) {   
					
					modelField
						 .append($("<option></option>")
						 .attr("value",key)
						 .text(value)); 
							 
						modelSelect.append($('<li><a rel="' + key + '" href="' + value + '">' + value + '</a></li>'));
						 
						
				});
				
			});
		}
	});
	
	$('.limit input[maxlength], .limit textarea[maxlength]').each(function() {

		var maxLen = $(this).attr('maxlength');	
		$(this).parent('span').after('<p class="charsRemaining">0 / ' + maxLen + ' characters remaining</p>');

		if($(this).val().length > maxLen){
			$(this).val($(this).val().substr(0, maxLen));
		}
		$(this).parent('span').siblings('.charsRemaining').text((maxLen - $(this).val().length) + ' / ' + maxLen + ' characters remaining');
		
		$(this).keyup(function(){
			if($(this).val().length > maxLen){
				$(this).val($(this).val().substr(0, maxLen));
			}
			$(this).parent('span').siblings('.charsRemaining').text((maxLen - $(this).val().length) + ' / ' + maxLen + ' characters remaining');
		});
		
	});

	if($('.accident-claims').length > 0)
	{
		$.ajax({
		  url: '/service/claims',
		  success: function(data) {
			data = jQuery.parseJSON(data);
			$('.accident-claims').removeClass('loading');	
			$('.accident-claims .accidents').text(data.accidents);
			$('.accident-claims .thefts').text(data.thefts);
			if(data.can_hire)
			{
				$('.accident-claims').addClass('passed');	
			}
			else
			{
				$('.accident-claims').addClass('failed');	
				$('.user-confirm').hide();
				$('input[type="image"]:not(.previous)').hide();
			}
		  }
		});
	}

	$('.select select').each(function() {
		var fId = $(this).attr('id');
		var parentX = $(this).parents('.select');
		var filterItems = '';
		var active = '';
		$('option', this).each(function() {
		// your normalizing script here
			if($(this).attr('selected') || !$(this).val())
			{
				active = $(this).attr('label');
			}
			filterItems += '<li><a href="#' + $(this).attr('label') + '" rel="' + $(this).val() + '">' + $(this).attr('label') + '</a></li>';
		})
		var filterObject = '<div><p class="list-heading"><em>' + active + '</em></p><ul data-rel="' + fId + '">' + filterItems + '</ul></div>';
		
		parentX.append(filterObject);
		$(this).hide();
		
	});	
	


	handleOccupation(false);

});


function handleOccupation(isFb)
{


	$( "#frm-occupation, #frm-occupation-fb" ).keypress(function (e){
		var keyCode = e.keyCode || e.which; 
		if (keyCode != 9) { 
			$(this).closest('.occupation').removeClass('has-errors').removeClass('ok');
			hasValidValue = 0;
		}
	});

	if(isFb)
	{

		$('.ui-autocomplete a').live('click',function() {

			$('#frm-occupation-fb').closest('.occupation').removeClass('has-errors').addClass('ok');
			hasValidValue = 1;
			clicked = true;

		});	

	}
	else
	{


		$('.ui-autocomplete a').live('click',function() {
			$('.occupation').removeClass('has-errors').addClass('ok');
			hasValidValue = 1;
			clicked = true;

		});	

	}

	$( "#frm-occupation, #frm-occupation-fb" ).on('blur',function (){

		var itemNode = $(this);
		$.ajax({
			type: "POST",
			url: "/api/check-occupation",
			data: { occupation: $(this).val()}
		}).done(function( response ) {

			if(clicked)
			{
				clicked = false;
				return false;
			}

			if(response.exists)
			{
				itemNode.closest('.occupation').removeClass('has-errors').addClass('ok');
				hasValidValue = 1;
			}
			else
			{
				itemNode.closest('.occupation').addClass('has-errors').removeClass('ok');
				hasValidValue = 0;
			}

		});


		if($(this).val() != '' && !hasValidValue)
		{
			$(this).closest('.occupation').addClass('has-errors').removeClass('ok');
		}
	});

	$( "#frm-occupation, #frm-occupation-fb" ).autocomplete({
		source: "/api/occupation-search",
		minLength: 2
	});

}


function facebookBinding(path,FBrequestUri)
{
	
	if(!FBrequestUri)
	{
		var FBrequestUri = '/';	
	}
	$('.fb').on('click','a',function() {
			FB.login(function(response) {
				if (response.authResponse) {
					FB.api('/me', function(response) {
						$.ajax({
		    			  	url: path + '/auth/facebook-register',
		    			  	type: 'POST',
							async: false,
		    		        data: {facebook: response, requestUri: FBrequestUri},
		    			  	success: function(result) {

								if(result == 1) // response 1 means we're all done with overlays, redirect them
								{
									window.location = FBrequestUri;
									return false;
								}
								else if(result == 2) // response 2 means something is not quite right, take them to manual registration
								{
									window.location = '/register';
									return false;
								}
								else // otherwise we have markup back meaning we need to go into the overlay
								{
									$('#dialog-modal').html(result);
	
									$( "#dialog-modal" ).dialog("open");

									$('#dialog-modal').on('click','.close',function(){
										$("#dialog-modal").dialog("close");
									});

									$( "#frm-occupation, #frm-occupation-fb" ).autocomplete({
										source: "/api/occupation-search",
										minLength: 2
									});

									handleOccupation(true);

									$('#dialog-modal fieldset').on('click','.terms',function() {
										$(this).siblings('ul.errors').slideUp();
									});

									$('#dialog-modal fieldset span').on('click','input',function() {
										$(this).siblings('ul.errors').slideUp();
										$(this).parents('section').children('ul.errors').slideUp();
									});
									
									$('.facebook-step').on('blur','.required input[type="text"]',{ linkType:'required', cssClass:'.required' },validateInput);
									$('.facebook-step').on('blur','.date input',{ linkType:'date', cssClass:'.date' },validateInput);
									$('.facebook-step').on('blur','.licence input',{ linkType:'licence', cssClass:'.licence' },validateInput);
									$('.facebook-step').on('blur','.mobile input',{ linkType:'mobile', cssClass:'.mobile' },validateInput);

									// Handle submits.
									$('#dialog-modal').on('click',"input[type='image']:not('.address-lookup,.find-occupation')",function() {
	
										$('form').append('<input type="hidden" name="' + $(this).attr('name') + '" value="1" />');
	
										var parentForm = $(this).closest('form');
	
										$.ajax({
											url: path + '/auth/facebook-register',
											type: 'POST',
											data: parentForm.serialize(),
											success: function(response) {
																
												if(response == 1) // response 1 means we're all done with overlays, redirect them
												{
													//window.location = FBrequestUri;
													window.location = '/account';
													return false;
												}
												else if(response == 2) // response 2 means something is not quite right, take them to manual registration
												{
													window.location = '/register';
													return false;
												}
												else // otherwise we have markup back meaning we need to go into the overlay
												{
												
													$('#dialog-modal').html(response);
													$( "#frm-occupation, #frm-occupation-fb" ).autocomplete({
														source: "/api/occupation-search",
														minLength: 2
													});

													handleOccupation(true);

													$('#dialog-modal fieldset').on('click','.terms',function() {
														$(this).siblings('ul.errors').slideUp();
													});

													$('#dialog-modal fieldset span').on('click','input',function() {
														$(this).siblings('ul.errors').slideUp();
														$(this).parents('section').children('ul.errors').slideUp();
													});

													$('#dialog-modal').on('click','.close',function(){
														$("#dialog-modal").dialog("close");
													});
									
													$('.facebook-step').on('blur','.required input[type="text"]',{ linkType:'required', cssClass:'.required' },validateInput);
													$('.facebook-step').on('blur','.date input',{ linkType:'date', cssClass:'.date' },validateInput);
													$('.facebook-step').on('blur','.licence input',{ linkType:'licence', cssClass:'.licence' },validateInput);
													$('.facebook-step').on('blur','.mobile input',{ linkType:'mobile', cssClass:'.mobile' },validateInput);

												}
											}
										});
	
										return false;
									});
									
								}
		    				}
		    			});
					});
				}
            }, {scope: 'email,user_education_history,user_hometown,user_likes,user_location,user_work_history,user_status,user_events,user_birthday'});
		return false;
	});
	
}

function bindAddressLookup()
{
	$('#addressLookupResponse').on('click','a',function() {
		
			var value = $(this).attr('rel');
	
			$.ajax({
				url: '/api/full-address/id/' + value,
				dataType: 'json',
				success: function(data) {
					
					$('input[name="addressLine1"],input[name="addressLine2"],input[name="city"],input[name="county"]').val('');
					
					$.each(data.result, function(key, value) {
						$('input[name="' + key + '"]').val(value).blur();
						$('input[name="' + key + '"]').closest('span').find('ul.errors').slideUp();				 
					});
					
				}
			});

		$(this).closest('ul').slideUp();			
		
		return false;
	})
}

function instantiateFacebook(appId,channelFile,fbLoggedIn,controller,action)
{
	// Facebook Connection functionality is started asynchronously to help
	// with page load speed.
	window.fbAsyncInit = function () {
		FB.init({
			appId      : appId, // App ID
			channelUrl : channelFile, // Channel File
			status     : true, // check login status
			cookie     : true, // enable cookies to allow the server to access the session
			xfbml      : true  // parse XFBML
		});

		// Handle logins via Facebook - we should do a single refresh
		// of the page the user has just logged into Facebook elsewhere
		// in the browser.
		FB.Event.subscribe('auth.login', handleLogin);
		function handleLogin(response) {
			var documentUrl = document.URL;
			if (response.authResponse && 0 == fbLoggedIn && !('auth' == controller && 'register' == action) && !('index' == controller && 'index' == action)) {
				//window.location = documentUrl;
				window.location = '/account';
			}
		}

		// Handle logouts via Facebook - we should make sure the user
		// is logged out of the if they log out of Facebook elsewhere
		// in the browser.
		FB.Event.subscribe('auth.logout', handleLogout);
		function handleLogout(response) {
			if (null == response.authResponse && 1 == fbLoggedIn) {
				window.location = '/auth/logout';
			}
		}
	};

	// Load the Facebook SDK Asynchronously
	(function (d) {
		var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		if (d.getElementById(id)) { return; }
		js = d.createElement('script'); js.id = id; js.async = true;
		js.src = "//connect.facebook.net/en_US/all.js";
		ref.parentNode.insertBefore(js, ref);
	} (document));

	// Function to call from the logout button.  Will only log the user
	// out of Facebook if they are logged in as a Facebook user.
	$('.user-info').on('click','.logout',function() {
		if (FB.getAuthResponse() && 1 == fbLoggedIn) {
			FB.logout(function(response) {
				window.location = '/auth/logout';
			});
		} else {
			window.location = '/auth/logout';
		}
		return false;
	});
	
}

function unlinkFacebookBinder(secureUrl)
{
	   
	$('#facebook-connected').on('click','.unlink-facebook',function() {
		$.ajax({
			url: secureUrl + '/auth/facebook-disconnect',
			type: 'GET',
			success: function(result) {
				$('#dialog-modal').html(result);

				$('#dialog-modal').on('click','.close',function(){
					$("#dialog-modal").dialog("close");
				});

				$( "#dialog-modal" ).dialog("open");

				// Handle submits.
				$('#dialog-modal').on('click','input[type="image"]',function() {
																			 
					if($(this).attr('name') == 'action-cancel')
					{
						$('#dialog-modal').dialog('close');                             
						return false;
					}

					$('form').append('<input type="hidden" name="' + $(this).attr('name') + '" value="1" />');

					var parentForm = $(this).closest('form');

					$.ajax({
						url: secureUrl + '/auth/facebook-disconnect',
						type: 'POST',
						data: parentForm.serialize(),
						success: function(response) {
							if(response) {
								$('#dialog-modal').html(response);
								$('#dialog-modal').on('click','.close',function(){
									$("#dialog-modal").dialog("close");
								});
							}
							else
							{
								// Successfully disconnected so lets reload the page.
								location.reload();
								return false;
							}
						}
					});
					
					return false;

				});
			}
		});
		
		return false;
	});

}

	
function doDialog()
{
	
	var Dwidth = 960;
	if($('.login .fb, .unlink-facebook').length) Dwidth = 640;
	if($('#triggerCalendar').length) Dwidth = 650;
	if($('.unlink-facebook').length) Dwidth = 470;
	
	$("#dialog-modal").dialog({
	autoOpen: false,
	width: Dwidth,
	modal: true,
        position: ['center',20],

	close:function() {
		$("#dialog-modal").dialog( "destroy" ).remove();
		$('body').append('<div id="dialog-modal"><div id="loading">Loading</div></div>');
		doDialog();
	}
	});	
}


function validateEmail(email) { 
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;    
	return re.test(email);
} 

function validateDate(date) { 
	var re = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;   
	return re.test(date);
} 

function validateMobile(number) {
	var re = /^(\s*\d){11}$/;
	return re.test(number);
}

function validateInput(event)
{

	if(event.data.linkType == 'email')
	{
		if($(this).closest('.emailaddress').hasClass('confirmaddress'))
		{
			var matchAgainst = $(this).closest(event.data.cssClass).siblings(event.data.cssClass).find('input').val();
			if($(this).val() != matchAgainst)
			{
				$('.confirmaddress').removeClass('ok').addClass('has-errors');
				return false;
			}
		}
	}

	if(event.data.linkType == 'password')
	{
		if($(this).closest('.password').hasClass('confirmpassword'))
		{
			var matchAgainst = $(this).closest(event.data.cssClass).siblings(event.data.cssClass).find('input').val();
			if($(this).val() != matchAgainst)
			{
				$('.confirmpassword').removeClass('ok').addClass('has-errors');
				return false;
			}
		}
	}
	
	if(!$(this).val())
	{
		$(this).closest(event.data.cssClass).removeClass('ok').addClass('has-errors');
		return;
	}

	if(event.data.linkType == 'mobile' && !validateMobile($(this).val()))
	{
		$(this).closest(event.data.cssClass).removeClass('ok').addClass('has-errors');
		return;
	}

	if(event.data.linkType == 'email' && !validateEmail($(this).val()))
	{
		$(this).closest(event.data.cssClass).removeClass('ok').addClass('has-errors');
		return;
	}

	if(event.data.linkType == 'date' && !validateDate($(this).val()))
	{
		$(this).closest(event.data.cssClass).removeClass('ok').addClass('has-errors');
		return;
	}
	var currentTime = new Date();

	if(event.data.linkType == 'dob')
	{
		var dd = $('.' + event.data.cssClass + ' input[placeholder="dd"]').val();
		var mm = $('.' + event.data.cssClass + ' input[placeholder="mm"]').val() -1;
		var yyyy = $('.' + event.data.cssClass + ' input[placeholder="yyyy"]').val();

		var date = dd + '/' + (mm +1) + '/' + yyyy;
		var dob = new Date(yyyy,mm,dd);
		var is18 = new Date(dob.getFullYear() + 18, dob.getMonth(), dob.getDate());
		if(!validateDate(date) || is18>=currentTime) {
			$(this).closest(event.data.cssClass).removeClass('ok').addClass('has-errors');
			return;
		}
	}

	
	if(event.data.linkType == 'licence')
	{
		var dd = $('.' + event.data.cssClass + ' input[placeholder="dd"]').val();
		var mm = $('.' + event.data.cssClass + ' input[placeholder="mm"]').val() -1;
		var yyyy = $('.' + event.data.cssClass + ' input[placeholder="yyyy"]').val();

		var dobdd = $('.dob input[placeholder="dd"]').val();
		var dobmm = $('.dob input[placeholder="mm"]').val() -1;
		var dobyyyy = $('.dob input[placeholder="yyyy"]').val();

		var date = dd + '/' + (mm+1) + '/' + yyyy;

		var dob = new Date(dobyyyy,dobmm,dobdd);
		var was17 = new Date(dob.getFullYear() + 17, dob.getMonth(), dob.getDate());

		var licenceDate = new Date(yyyy,mm,dd);

		if(!validateDate(date) || was17>=licenceDate) {
			$(this).closest(event.data.cssClass).removeClass('ok').addClass('has-errors');
			return;
		}
	}

	var passregex = /\d/g;
	if(event.data.linkType == 'password' && $(this).val().length < 6 || event.data.linkType == 'password' && (passregex.test($(this).val()) == false))
	{
		$(this).closest(event.data.cssClass).removeClass('ok').addClass('has-errors');
		return;
	}

	$(this).closest(event.data.cssClass).removeClass('has-errors').addClass('ok');

	$(this).siblings('ul.errors').remove();
}


function bindBookingMaps()
{
	
	var mapid = $('.map').not('.activated').attr('data-id');

	var myLatlng = new google.maps.LatLng($('#map' + mapid).attr('data-lat'), $('#map' + mapid).attr('data-lng'));
	var myOptions = {
		zoom: 12,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		scrollwheel: false
	}

	var map = new google.maps.Map(document.getElementById("map" + mapid), myOptions);

	var marker = new google.maps.Marker({
		position: myLatlng, 
		map: map
	});   
	
	marker.setIcon('/static/images/bg-map-icon-a.png', [32,41]); 

	$('#map' + mapid).addClass('activated');

}

var clickFlag = false;
var clickTimer;
function clickOnce() {

	if(clickFlag == false)
	{
		clickFlag = true;
		$('.clickOnce').after('<img src="/static/images/btn-confirm-lg-loading.png" alt="Loading..." class="loading-btn" />');
		$('.clickOnce').hide();
		clickTimer = setTimeout("resetClickFlag()",5000);
	}
	else 
	{
		return false;
	}
};

function resetClickFlag()
{
	clearTimeout(clickTimer);
	$('.loading-btn').remove();
	$('.clickOnce').show();
	clickFlag = false;
}
